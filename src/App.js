import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ShowsList from './ShowsList/ShowsList';
import ShowPreview from './ShowPreview/ShowPreview';

function App() {
  return (
    <div>
    <Router>
        <div className="container">
          <Route  exact path="/" component={ShowsList}></Route>
          <Route  exact path="/show/:id" component={ShowPreview}></Route>
        </div>
        
    </Router>
    </div>
    
   
  );
}

export default App;
