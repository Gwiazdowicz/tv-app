import React from 'react';
import './ShowsList.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Spinner from '../Spinner/Spinner'

export class ShowsList extends React.Component {

  constructor(){
    super();

    this.state = {
      shows : [],
    }  

    this.setTitleFilter = this.setTitleFilter.bind(this)
  }
  componentDidMount(){
    axios.get("http://api.tvmaze.com/shows").then(res => {
      let shows = res.data.map(show => {
      return{
        id:show.id,
        name:show.name,
        desc:show.summary.replace(/(<([^>]+)>)/ig, '').substring(0,100) + " ...",
        img: show.image.medium
      }  
      })
      this.setState({shows, showToDisplay: shows})
    })
  }

  displayShows(){
    if(this.state.showToDisplay === 0){
      return <div className="loader-container">There are no shows matching yours filter</div>
    } else{
      if (this.state.shows.length > 0){
        return this.state.showToDisplay.map((show, key) => (
           <div className="card" key={key} >
             <img src={show.img} className="card-img-top" alt="Postery"></img>
             <div className="card-body">
               <h5 className="card-title">{show.name}</h5>
               <p className="card-text">{show.desc}</p>
               <Link className="btn btn-primary" to={`show/${show.id}`}>Read more ...</Link>
             </div>
           </div>
       ))
     } else{
       return(
         <div className="loader-container">
         <Spinner></Spinner>
         </div>
       )  
     }
    }
  }

  setTitleFilter(ev){
    const { shows } = this.state;
    let showToDisplay = shows.filter(show => {
      return show.name.toLowerCase().startsWith(ev.target.value.toLowerCase())
    })
    this.setState({showToDisplay})
  }

  render(){
    return (
      <div>
        <div className= "shows-search">
          <h4>Find your favorite show!</h4>
          <input type="text" placeholder="Insert your title here" onKeyUp={this.setTitleFilter}></input>
        </div>
        <section className="shows-list">
            {this.displayShows()}
        </section>
      </div>);
  }
}

export default ShowsList ;
