import React from 'react';
import './ShowPreview.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Spinner from '../Spinner/Spinner';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faStar,  faStarHalfAlt} from "@fortawesome/free-solid-svg-icons";


export class ShowPreview extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      selectedShow: null,
    }
    this.displayShowPreview = this.displayShowPreview.bind(this);
  }

  displayHeader(selectedShow){
    return <header className="show-header" style={{backgroundImage: "url('" + selectedShow.image.original +"')" }}></header>
  }

  displayMainInfo(selectedShow){
    return(
    <main className= "show-about">  
      <h2>{selectedShow.name}</h2>
      <h2>{selectedShow.rating.average + " "}{this.displayStars(selectedShow)}{this.noDisplayStars(selectedShow)} </h2>
      <h5 className="genres">{this.displayGenres(selectedShow)}</h5>
      <h5>Premiered: {selectedShow.premiered}</h5>
      <div className="cool-o-meter">
        <div style={{width: selectedShow.rating.average * 10 + "%"}}></div>
      </div>
      <h5 className="about-show-text">{selectedShow.summary.replace(/(<([^>]+)>)/ig, '')}</h5>
    </main>)
  }

  displayGenres(){
    let table = []
    const {selectedShow} = this.state;
    for (let i = 0 ; i < selectedShow.genres.length; i++){
      console.log(selectedShow.genres.length);
      table.push(selectedShow.genres[i] + " ")} 
      return table;
}

  displayStars(){
    let table = []
    const {selectedShow} = this.state;
    for (let i = 0 ; i < Math.floor(selectedShow.rating.average); i++){
      table.push( <FontAwesomeIcon className="star-icon" key={table.length} icon={faStar}/>)}
      if( Math.floor(selectedShow.rating.average) === selectedShow.rating.average)
      return table;
      else {table.splice(table.length);
      table.push(<FontAwesomeIcon  className="star-icon" key={table.length} icon={faStarHalfAlt}/>)}
      return table;
      } 
    
  noDisplayStars(){
    let tab = []
    const {selectedShow} = this.state;
    for (let i = 0 ; i < 10 - Math.floor(selectedShow.rating.average); i++){
      tab.push(<FontAwesomeIcon className="no-star-icon" key={tab.length} icon={faStar}/>)}
      if( Math.floor(selectedShow.rating.average) === selectedShow.rating.average)
      return tab;
      else {tab.splice(tab.length-1);
      return tab;
      } 
    }

  displayShowPreview(){
    const {selectedShow} = this.state;

    return (
      <section className="show-preview">
        {this.displayHeader(selectedShow)}
        {this.displayMainInfo(selectedShow)}
       </section>
    )
  }

  render(){
    let show;

    if(this.state.selectedShow) {
      show = this.displayShowPreview();
    }else {
      show = <div className="loader-container">
      <Spinner></Spinner>
    </div>
    }
  return(
    <div>
      <Link to= "/"><button className="btn btn-warning">Return</button></Link>
      <h1>{show}</h1>
    </div>
  )}
  
  componentDidMount(){
    axios.get(`http://api.tvmaze.com/shows/${this.props.match.params.id}`).then(show => {
      this.setState({
        selectedShow: show.data
      }) 
    })
  }
}
export default ShowPreview ;
